// Projects: Example | Tokenize example in nodejs

// File description: Tokenize flow to pay request orders.

// Author: mumana@greenpay.me

// Versions:
//     2018-08-09 :   Firts release @greenpay/Tokenize_nodejs.

// Required libs

// Import enviroment var's

// See .env.default and configure it with the credentials provided by Greenpay support team. Also change the path to your .env file
require("dotenv").config()

// Import nodeJs packages
const rsa = require("node-jsencrypt")
// eslint-disable-next-line new-cap
const rsa_ = new rsa()
const aesjs = require("aes-js")
const unirest = require("unirest")
const KJUR = require("jsrsasign")

var requestId = "xwr-123455"

/**
     * Creates a AES key pairs
     */
function generateAESPairs () {
    var key = []
    var iv
    for (let k = 0; k < 16; k++) {
        key.push(Math.floor(Math.random() * 255))
    }
    for (let k = 0; k < 16; k++) {
        iv = Math.floor(Math.random() * 255)
    }

    return {
        k: key,
        s: iv
    }
}

// Creates an JSON object with the card data and AES key Pair encrypted
function pack (obj, session, pair_) {
    var pair = (pair_ !== undefined) ? pair_ : generateAESPairs()

    var textBytes = aesjs.utils.utf8.toBytes(JSON.stringify(obj))
    // eslint-disable-next-line new-cap
    var aesCtr = new aesjs.ModeOfOperation.ctr(pair.k, new aesjs.Counter(pair.s))
    var encryptedBytes = aesCtr.encrypt(textBytes)
    var encryptedHex = aesjs.utils.hex.fromBytes(encryptedBytes)

    var returnObj = {
        session: session,
        ld: encryptedHex,
        lk: rsa_.encrypt(JSON.stringify(pair))
    }
    console.log("Pack: ", JSON.stringify(returnObj), "\n")
    return returnObj
}

// Tokenize the order creates in Greenpay gateway
function postTokenize (data, accessToken) {
    return new Promise(function (resolve, reject) {
        unirest.post(process.env.CHECKOUT_ENDPOINT + "/tokenize")
            .headers({
                "Accept": "application/json",
                "Content-Type": "application/json",
                "liszt-token": accessToken
            })
            .send(data)
            .end(function (response) {
                if (response.status === 200) {
                    resolve(response.body)
                } else {
                    reject(response.body)
                }
            })
    })
}

// Creates a payment order in Greenpay Gateway
function postCreateOrder (data) {
    return new Promise(function (resolve, reject) {
        unirest.post(process.env.TOKENIZE_ENDPOINT)
            .headers({
                "Accept": "application/json",
                "Content-Type": "application/json"
            })
            .send(JSON.stringify(data))
            .end(function (response) {
                if (response.status === 200) {
                    resolve(response.body)
                } else {
                    reject(response.body)
                }
            })
    })
}

const tokenizeOrder = async (cardData, security) => {
    let resp = {}
    // try {
    rsa_.setPublicKey(process.env.PUBLIC_KEY)
    const body = pack(cardData, security.session)
    resp = await postTokenize(body, security.token)
    return resp
}

const verify = (signed, toVerify) => {
    const sig = new KJUR.crypto.Signature({
        "alg": "SHA256withRSA"
    })
    // You should verify with your public key in the PEM format
    sig.init(`-----BEGIN PUBLIC KEY-----${process.env.PUBLIC_KEY}-----END PUBLIC KEY-----`)
    sig.updateString(toVerify)
    return sig.verify(signed)
}

// Main function
handler = async (cardData, orderRequestData) => {
    let security = await postCreateOrder(orderRequestData) // Create an order in greenpay systema. Receive an object {session: "xxx",token:"xxx"}
    console.log("security: ", JSON.stringify(security), "\n")
    let resp = {}

    try {
        resp = await tokenizeOrder(cardData, JSON.parse(JSON.stringify(security))) // tokenize order before created 
        await console.log("GreenPay response: ", JSON.stringify(resp, null, 2), "\n")
        const toVerify = `status:${resp.status},requestId:${resp.requestId}`
        const verified = verify(resp._signature, toVerify) // Verify signature returned in payOrder() response.
        if (verified) {
            resp = {
                status: 200,
                description: "Successful"
            }
        } else {
            throw Error("Response was not received from greenpay!")
        }
    } catch (err) {
        resp = {
            "status": 500
        }
        resp.errors = [err]
    }
    console.log("Verification signature: ", JSON.stringify(resp, null, 2))
    return resp
}

// Car data
// Send card data, generate random number form http://www.getcreditcardnumbers.com/
const cardData = {
    "card": {
        "cardHolder": "Name",
        "expirationDate": {
            "month": 9,
            "year": 21
        },
        "cardNumber": "", // 4485970366452449 DENEGADA, 4777777777777777 OK
        "cvc": "",
        "nickname": "Some nickname"
    }
}

// Order data
const orderRequestData = {
    "secret": process.env.SECRET,
    "merchantId": process.env.MERCHANT_ID,
    "requestId": requestId
}

// Call to main fuction
handler(cardData, orderRequestData)
